---
title: Workshop first post
date: 2017-10-06
---
# Voorbeeld post

Dit is een voorbeeld post voor de *workshop*

## Done
Wat we hebben gedaan is:
1. Gitlab aangemaakt
2. hugo geforkt
3. Settings aangepast
4. Blogpost verwijderd, wat heel veel werk was
5. Een nieuwe post online gezet

## en verder
Wat we nog moeten doen:
* Thema wijzigen
* Nog meer settings
* Meer bloggen
* _Iets wat cursief is_
* en nog meer